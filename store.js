import { useMemo } from "react";
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";

let store;

const initialState = {
  userData: null,
  isLoggedIn: false,
  authToken: null,
  initialLoaded: false,
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case "LOGIN": {
      localStorage.setItem("authToken", payload.token);
      localStorage.setItem("userData", JSON.stringify(payload.userData));
      return {
        ...state,
        isLoggedIn: true,
        authToken: payload.token,
        userData: payload.userData,
      };
    }
    case "LOGOUT": {
      localStorage.clear();
      return;
    }
    case "AUTHENTICATED_INITIAL": {
      return {
        ...state,
        isLoggedIn: payload.isLoggedIn,
        initialLoaded: true,
        userData: payload.userData,
      };
    }
    default:
      return state;
  }
};

function initStore(preloadedState = initialState) {
  return createStore(
    reducer,
    preloadedState,
    composeWithDevTools(applyMiddleware())
  );
}

export const initializeStore = (preloadedState) => {
  let _store = store ?? initStore(preloadedState);

  // After navigating to a page with an initial Redux state, merge that state
  // with the current state in the store, and create a new store
  if (preloadedState && store) {
    _store = initStore({
      ...store.getState(),
      ...preloadedState,
    });
    // Reset the current store
    store = undefined;
  }

  // For SSG and SSR always create a new store
  if (typeof window === "undefined") return _store;
  // Create the store once in the client
  if (!store) store = _store;

  return _store;
};

export function useStore(initialState) {
  const store = useMemo(() => initializeStore(initialState), [initialState]);
  return store;
}
