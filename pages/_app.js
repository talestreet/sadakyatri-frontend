import "@/styles/scss/soft-design-system.scss";
// import '../styles/globals.scss';
import Head from "next/head";
import Header from "@/components/Header";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Provider, Connect } from "react-redux";
import { useStore } from "../store";
import React, { useEffect } from "react";
import App from "@/containers/App";
import { Router, useRouter } from "next/router";
import { route } from "next/dist/next-server/server/router";
function MyApp({ Component, pageProps }) {
  const store = useStore(pageProps.initialReduxState);

  return (
    <div>
      <Head>
        <link
          href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
          rel="stylesheet"
        />
        <link href="/assets/css/nucleo-icons.css" rel="stylesheet" />
        <link href="/assets/css/nucleo-svg.css" rel="stylesheet" />
        <script
          src="https://kit.fontawesome.com/70ea2d195e.js"
          crossorigin="anonymous"
        ></script>
      </Head>
      <Provider store={store}>
        <App Component={Component} pageProps={pageProps}></App>
      </Provider>
    </div>
  );
}

// MyApp.getInitialProps = async ({ req }) => {
//   // return {}
//   // let token;
//   if (req) {
//     return {
//       'login':true
//     }
//   //   // server
//   //   return { page: {} };
//   } else {
//     return {
//       login:false
//     }
//   //   // client
//   //   const token = localStorage.getItem("auth");
//   //   const res = await fetch(`${process.env.API_URL}/pages/about`, {
//   //     headers: { Authorization: token },
//   //   });
//   //   const data = await res.json();
//   //   return { page: data };
//   }
// };

export default MyApp;
