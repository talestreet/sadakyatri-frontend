import Head from "next/head";
import styles from "@/styles/Home.module.css";
import { Alert } from "reactstrap";
import Categories from '@/components/Categories';
import HomeCTA from '@/components/HomeCTA';
import Footer from '@/components/Footer';
export default function Home() {
  return (
    <div>
      <Head>
        <title>l</title>
      </Head>

      <main>
        <header>
          
          <div
            className="page-header section-height-75"
            style={{
              backgroundImage:
                "url(https://images.unsplash.com/photo-1587031654423-db32ee2c57e3?ixlib=rb-1.2.1&auto=format&fit=crop&w=1920&q=80)",
            }}
          >
            <span className="mask bg-gradient-dark" />
            <div className="container">
              <div className="row">
                <div className="col-lg-8 mx-auto text-white text-center">
                  <h2 className="text-white">Book you not the trip</h2>
                  <p className="lead">
                    An arrangement you make to have a hotel room, tickets, etc.
                    at a particular time in the future:
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div
            className="position-relative overflow-hidden"
            style={{ height: "36px", marginTop: "-35px" }}
          >
            <div
              className="w-full absolute bottom-0 start-0 end-0"
              style={{
                transform: "scale(2)",
                transformOrigin: "top center",
                color: "#fff",
              }}
            >
              <svg
                viewBox="0 0 2880 48"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z"
                  fill="currentColor"
                />
              </svg>
            </div>
          </div>
          <div className="container">
            <div className="row bg-white shadow-lg mt-n6 border-radius-md pb-4 p-3 mx-sm-0 mx-1 position-relative">
              <div className="col-lg-3 mt-lg-n2 mt-2">
                <label className>Leave From</label>
                <select
                  className="form-control"
                  name="choices-button"
                  id="choices-button"
                  placeholder="Departure"
                >
                  <option value="Choice 1" selected>
                    Brazil
                  </option>
                  <option value="Choice 2">Bucharest</option>
                  <option value="Choice 3">London</option>
                  <option value="Choice 4">USA</option>
                </select>
              </div>
              <div className="col-lg-3 mt-lg-n2 mt-2">
                <label className>To</label>
                <select
                  className="form-control"
                  name="choices-remove-button"
                  id="choices-remove-button"
                  placeholder="Destination"
                >
                  <option value="Choice 1" selected>
                    Italy
                  </option>
                  <option value="Choice 2">Spain</option>
                  <option value="Choice 3">Denmark</option>
                  <option value="Choice 4">Poland</option>
                </select>
              </div>
              <div className="col-lg-3 mt-lg-n2 mt-2">
                <label className>Depart</label>
                <div className="input-group">
                  <span className="input-group-text">
                    <i className="fas fa-calendar" />
                  </span>
                  <input
                    className="form-control datepicker"
                    placeholder="Please select date"
                    type="text"
                  />
                </div>
              </div>
              <div className="col-lg-3 d-flex align-items-center my-auto">
                <button
                  type="button"
                  className="btn bg-gradient-dark w-100 btn-lg mb-0 m4 mt-3"
                >
                  Search
                </button>
              </div>
            </div>
          </div>
        </header>
        <Categories/>

        <HomeCTA/>
        <Footer/>
      </main>
    </div>
  );
}
