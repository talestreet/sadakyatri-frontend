import Head from "next/head";
import { Alert } from "reactstrap";
import styles from "@/styles/Profile.module.scss";
import { useState } from "react";
import Link from "next/link";
import Posts from "@/components/Posts";
export default function Home() {
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [processing, setProcesing] = useState(false);
  return (
    <div>
      <Head>
        <title>Login</title>
      </Head>

      <main className={`${styles.main}`}>
        <div className="row py-5 px-4">
          <div className="col-md-12 col-sm-12 mx-auto">
            {/* Profile widget */}
            <div className="bg-white shadow rounded overflow-hidden">
              <div className={`px-4 pt-0 pb-4 ${styles.cover}`}>
                <div className={`media align-items-end ${styles.profileHead}`}>
                  <div className="profile mr-3">
                    <img
                      src="https://images.unsplash.com/photo-1522075469751-3a6694fb2f61?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=80"
                      alt="..."
                      width={130}
                      className="rounded mb-2 img-thumbnail"
                    />
                  </div>
                  <div className="media-body mb-5 text-white">
                    <h4 className="mt-0 mb-0">Mark Williams</h4>
                    <p className="small mb-4">
                      {" "}
                      <i className="fas fa-map-marker-alt mr-2" />
                      New York
                    </p>
                  </div>
                </div>
              </div>

              <div className="py-4 px-4">
                <div className="d-flex align-items-center justify-content-between mb-3">
                  <h5 className="mb-0">Posts</h5>
                </div>

                <div className="d-flex align-items-center justify-content-between mb-3">
                  <h5 className="mb-0">Posts</h5>
                </div>
                <Posts></Posts>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
