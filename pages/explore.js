import Head from "next/head";
import { Alert } from "reactstrap";
import styles from "@/styles/Explore.module.scss";
import { useState } from "react";
import Link from "next/link";
import Posts from "@/components/Posts";
export default function Home() {
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [processing, setProcesing] = useState(false);
  return (
    <div>
      <Head>
        <title>Login</title>
      </Head>

      <main className={`${styles.main}`}>
        <div className={styles.bbbootstrap}>
          <div className={styles.container}>
            <form className={styles.form}>
              <span
                role="status"
                aria-live="polite"
                classname="ui-helper-hidden-accessible"
              >
                <input
                  type="text"
                  id="Form_Search"
                  defaultvalue
                  placeholder="Search for your best result in our community"
                  role="searchbox"
                  className={styles.InputBox}
                  autoComplete="off"
                />
                <input
                  type="submit"
                  id="Form_Go"
                  className={styles.Button}
                  defaultvalue="GO"
                />
              </span>
            </form>
          </div>
        </div>

        <div className="row py-5 px-4">
          <div className="col-md-5 mx-auto">
            {/* Profile widget */}
            <div className="bg-white shadow rounded overflow-hidden">
              <Posts></Posts>
              <Posts></Posts>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
