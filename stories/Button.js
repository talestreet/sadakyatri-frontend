import React from "react";
import PropTypes from "prop-types";
import "./button.scss";
import { Button as Rbutton } from "reactstrap";
/**
 * Primary UI component for user interaction
 */
export const Button = ({
  type = "primary",
  outline = false,
  size = null,
  label,
  ...props
}) => {
  return (
    <Rbutton
      type="button"
      outline={outline}
      size={size}
      color={type}
      className={["storybook-button"].join(" ")}
      {...props}
    >
      {label}
    </Rbutton>
  );
};

Button.propTypes = {
  /**
   * Is this the principal call to action on the page?
   */
  type: PropTypes.oneOf(["primary", "secondary", "danger", "warning"]),
  /**
   * What background color to use
   */
  backgroundColor: PropTypes.string,
  /**
   * How large should the button be?
   */
  size: PropTypes.oneOf(["sm", "md", "lg"]),
  /**
   * Button contents
   */
  label: PropTypes.string.isRequired,
  /**
   * Optional click handler
   */
  onClick: PropTypes.func,
};

Button.defaultProps = {
  type: "primary",
  size: "medium",
  onClick: undefined,
};
