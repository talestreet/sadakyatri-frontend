import React from "react";

import { Button } from "./Button";

export default {
  title: "Example/Button",
  component: Button,
  argTypes: {
    backgroundColor: { control: "color" },
  },
};

const Template = (args) => <Button {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  type: "primary",
  label: "Primary Button",
};

export const Danger = Template.bind({});
Danger.args = {
  type: "danger",
  label: "Danger Button",
};

export const Large = Template.bind({});
Large.args = {
  size: "large",
  label: "Button Large",
};

export const Small = Template.bind({});
Small.args = {
  size: "small",
  label: "Button Small",
};
