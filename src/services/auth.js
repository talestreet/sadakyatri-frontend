const axios = require("axios");
const BASE_URL = "http://18.136.101.225:3000";
const login = ({ params }) => {
  return axios.post(BASE_URL + "/user/login", params);
};

const signup = ({ params }) => {
  return axios.post(BASE_URL + "/user/signup", params);
};

export { login, signup };
