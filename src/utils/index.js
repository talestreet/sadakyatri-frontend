import { toast as libToast } from "react-toastify";
import { useSelector } from "react-redux";
const toast = (message) => {
  libToast(message);
};
const useAuthentication = () => {
  const isLoggedIn = useSelector((state) => state.isLoggedIn);
  return isLoggedIn;
};

export { toast, useAuthentication };
