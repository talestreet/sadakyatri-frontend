import Header from "@/components/Header";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Provider, connect, useSelector } from "react-redux";
import { Router, useRouter } from "next/router";
import React, { useEffect } from "react";

function MyApp({ Component, pageProps, dispatch }) {
  const appLoaded = useSelector((state) => state.initialLoaded);
  const isLoggedIn = useSelector((state) => state.isLoggedIn);
  const { pathname, push } = useRouter();

  useEffect(() => {
    if (localStorage.getItem("authToken")) {
      dispatch({
        type: "AUTHENTICATED_INITIAL",
        payload: {
          isLoggedIn: true,
          userData: JSON.parse(localStorage.getItem("userData")),
        },
      });
    } else {
      dispatch({
        type: "AUTHENTICATED_INITIAL",
        payload: {
          isLoggedIn: false,
          userData: null,
        },
      });
    }
  }, []);

  useEffect(() => {
    if (!appLoaded) return;
    if (isLoggedIn && ["/login", "/signup"].indexOf(pathname) > -1) {
      return push({ pathname: "/" });
    }
    if (!isLoggedIn && pathname.indexOf("profile") > -1) {
      push({ pathname: "/login" });
    }
  }, [appLoaded]);
  if (!appLoaded) {
    return "loading...";
  }
  return (
    <>
      <Header></Header>
      <ToastContainer></ToastContainer>
      <Component {...pageProps} />
    </>
  );
}
function mapStateToProps() {
  return {};
}
export default connect(mapStateToProps)(MyApp);
