import React, { useState } from "react";
const HomeCTA=()=>{
	return (
	<div className="position-relative mt-8">
  <div className="container">
    <div className="card bg-gradient-primary z-index-3">
      <div className="card-body">
        <div className="row">
          <div className="col-md-8 mx-auto text-center">
            <h3 className="text-white">Be the first who see the news</h3>
            <p className="text-white opacity-8 mb-5 pe-5">
              Your company may not be in the software business,
              but eventually, a software company will be in your business.
            </p>
            <div className="row">
              <div className="col-sm-5 ms-auto">
                <div className="input-group">
                  <span className="input-group-text"><i className="fas fa-envelope" /></span>
                  <input type="text" className="form-control mb-sm-0 mb-2" placeholder="Email Here..." />
                </div>
              </div>
              <div className="col-sm-3 me-auto text-left">
                <button type="button" className="btn bg-white w-100 mb-0 h-100 position-relative z-index-2">Subscribe</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

)
}
export default HomeCTA;