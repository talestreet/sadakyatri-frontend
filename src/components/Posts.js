import React from "react";
const Posts = () => {
  return (
    <div className="row">
      <div className="col-lg-4 col-sm-6 mb-2 pr-lg-1">
        <img
          src="https://images.unsplash.com/photo-1469594292607-7bd90f8d3ba4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"
          alt=""
          className="img-fluid rounded shadow-sm"
        />
      </div>
      <div className="col-lg-4 col-sm-6 mb-2 pl-lg-1">
        <img
          src="https://images.unsplash.com/photo-1493571716545-b559a19edd14?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"
          alt=""
          className="img-fluid rounded shadow-sm"
        />
      </div>
      <div className="col-lg-4 col-sm-6 pr-lg-1 mb-2">
        <img
          src="https://images.unsplash.com/photo-1453791052107-5c843da62d97?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
          alt=""
          className="img-fluid rounded shadow-sm"
        />
      </div>
      <div className="col-lg-4 col-sm-6 pl-lg-1">
        <img
          src="https://images.unsplash.com/photo-1475724017904-b712052c192a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"
          alt=""
          className="img-fluid rounded shadow-sm"
        />
      </div>
    </div>
  );
};
export default Posts;
