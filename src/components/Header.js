import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText,
} from "reactstrap";
import Link from "next/link";
import { useRouter } from "next/router";
import { useSelector, useDispatch } from "react-redux";
const Example = (props) => {
  const isLoggedIn = useSelector((state) => state.isLoggedIn);
  const userData = useSelector((state) => state.userData);
  const dispatch = useDispatch();
  const [isOpen, setIsOpen] = useState(false);
  const logout = () => {
    dispatch({ type: "LOGOUT" });
    window.location.reload();
  };
  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Navbar color="light" light expand="md">
        <NavbarBrand href="/">reactstrap</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            {isLoggedIn && (
              <>
                <li class="nav-item">
                  <a class="nav-link" onClick={logout}>
                    Logout
                  </a>
                </li>

                <li class="nav-item">
                  <Link href="/profile">
                    <a class="nav-link">Profile</a>
                  </Link>
                </li>
                <li class="nav-item">
                  <Link href="/profile/edit">
                    <a class="nav-link">Profile Edit</a>
                  </Link>
                </li>
              </>
            )}

            {!isLoggedIn && (
              <>
                <li class="nav-item">
                  <Link href="/login">
                    <a class="nav-link">login</a>
                  </Link>
                </li>
                <li class="nav-item">
                  <Link href="/signup">
                    <a class="nav-link">Signup</a>
                  </Link>
                </li>
              </>
            )}

            <li class="nav-item">
              <Link href="/profile/posts">
                <a class="nav-link">Posts</a>
              </Link>
            </li>
            <li class="nav-item">
              <Link href="/explore">
                <a class="nav-link">Explore</a>
              </Link>
            </li>
          </Nav>
        </Collapse>
        {userData && (
          <div>
            <Link href={`/profile`}>
              <a>{userData.email}</a>
            </Link>
          </div>
        )}
      </Navbar>
    </div>
  );
};

export default Example;
